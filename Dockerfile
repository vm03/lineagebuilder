FROM ubuntu:20.04

RUN apt-get update \
    && DEBIAN_FRONTEND="noninteractive" apt-get install -y \
        bc \
        bison \
        build-essential \
        ccache \
        curl \
        flex \
        g++-multilib \
        gcc-multilib \
        git \
        git-lfs \
        gnupg \
        gperf \
        imagemagick \
        lib32readline-dev \
        lib32z1-dev \
        libelf-dev \
        liblz4-tool \
        libsdl1.2-dev \
        libssl-dev \
        libxml2 \
        libxml2-utils \
        lzop \
        pngcrush \
        rsync \
        schedtool \
        squashfs-tools \
        xsltproc \
        zip \
        zlib1g-dev \
        wget \
        python-is-python2 \
        libwxgtk3.0-gtk3-dev \
        openjdk-8-jdk \
        libncurses5

RUN sed -i 's/^jdk.tls.disabledAlgorithms.*/jdk.tls.disabledAlgorithms=SSLv3, \\/g' /etc/java-8-openjdk/security/java.security
RUN curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/bin/repo \
    && chmod a+x /usr/bin/repo
RUN useradd -rm -s /bin/bash -u 1000 builder

USER builder

